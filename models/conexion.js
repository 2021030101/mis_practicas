import mysql from "mysql2";

var conexion = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "micontraseña",
    database: "Sistemas",
});

conexion.connect(function(err) {
    if(err) console.log("Surgió un error al conectar", err);
    else console.log("Se conectó con éxito 😊");
});

export default conexion;