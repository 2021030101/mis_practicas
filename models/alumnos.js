import conexion from "./conexion.js";
import { rejects } from "assert";

var alumnosDb = {};

alumnosDb.insertar = function insertar(alumno) {
    return new Promise((resolve, rejects) => {
        // consulta
        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta, alumno, function (err, res) {

            if (err) {
                console.log("Surgio un error ", err.message);
                rejects(err);
            }
            else {
                const alumno = {
                    id: res.id,
                }
                resolve(alumno);
            }
        });
    });
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta, null, function (err, res) {
            if (err) {
                console.log("Surgio un error");
                reject(err);

            }
            else {
                resolve(res);
            }
        });
    });
}

alumnosDb.buscarMatricula = function(matricula) {
    return new Promise((resolve, reject) => {
        const consulta = "SELECT * FROM alumnos WHERE matricula = ?";
        conexion.query(consulta, [matricula], function(err, res) {
            if(err) {
                console.log("Surgió un error: ", err);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

alumnosDb.borrar = function(matricula) {
    return new Promise((resolve, reject) => {
        const consulta = "DELETE FROM alumnos WHERE matricula = ?";
        conexion.query(consulta, [matricula], function(err, res) {
            if(err) {
                console.log("Surgió un error: ", err);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

alumnosDb.actualizar = function(matricula, nombre, domicilio, especialidad, sexo) {
    return new Promise((resolve, reject) => {
        const consulta = "UPDATE alumnos SET nombre = ?, domicilio = ?, especialidad = ?, sexo = ? WHERE matricula = ?";
        conexion.query(consulta, [nombre, domicilio, especialidad, sexo, matricula], (err, res) => {
            if(err) {
                console.log("Surgió un error");
                reject(res);
            }
            else {
                resolve(res);
            }
        });
    });
}

export default alumnosDb;