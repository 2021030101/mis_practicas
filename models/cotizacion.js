export default class Cotizacion {
    constructor(folio = '', descripcion = '', precio = 0, porcentaje = 0, plazo = 0) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentaje = porcentaje;
        this.plazo = plazo;
    }

    get pagoInicial() {
        return (this.precio * this.porcentaje / 100).toFixed(2) * 1;
    }

    get totalFinanciar() {
        return (this.precio - this.pagoInicial).toFixed(2) * 1;
    }

    get pagoMensual() {
        return (this.totalFinanciar / this.plazo).toFixed(2) * 1;
    }
}