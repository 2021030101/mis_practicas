import express from "express";
import json from "body-parser";
import conexion from "../models/conexion.js";
import Cotizacion from "../models/cotizacion.js";
import alumnosDb from "../models/alumnos.js";
export const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', {
        titulo: "Mi primer página ejs",
        nombre: "Yohan Alek Plazola Arangure"
    });
});

// Tabla Routes
router.get('/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    };
    res.render('tabla', params);
});
router.post('/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    };
    res.render('tabla', params);
});

// Cotización Routes
router.get('/cotizacion', (req, res) => {
    const { folio, descripcion, precio, porcentaje, plazos } = req.body;
    const cotizacion = new Cotizacion(folio, descripcion, precio, porcentaje, plazos);
    res.render('cotizacion', { cotizacion });
});
router.post('/cotizacion', (req, res) => {
    const { folio, descripcion, precio, porcentaje, plazos } = req.body;
    const cotizacion = new Cotizacion(folio, descripcion, precio, porcentaje, plazos);
    res.render('cotizacion', { cotizacion });
});

async function prueba() {
    try {
        const res = await conexion.execute("select * from alumnos");
        console.log(res);
    } catch (err) {
        console.log("Surgió un error: " + err);
    }
    finally {

    }
}

// Alumnos Routes
let rows;
router.get('/alumnos', async (req, res) => {
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos', { reg: rows });
    console.log(alumnosDb.mostrarTodos());
});

let params;
router.post('/alumnos', async (req, res) => {
    try {

        params = {
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        }
        const registros = await alumnosDb.insertar(params);
        console.log("-------------- registros " + registros);

    } catch (error) {
        console.error(error)
        res.status(400).send("sucedio un error: " + error);
    }
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos', { reg: rows });
});

router.post("/buscar", async (req, res) => {
    const matricula = req.body.matricula;
    const nrow = await alumnosDb.buscarMatricula(matricula);
    res.render("alumnos", {alu:nrow});
});

export default { router };